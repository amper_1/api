//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyparser = require('body-parser')
  app.use(bodyparser.json());
  app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  })

  function encriptar(user, pass) {
    var crypto = require('crypto')
    // usamos el metodo CreateHmac y le pasamos el parametro user y actualizamos el hash con la password
    var hmac = crypto.createHmac('sha1', user).update(pass).digest('hex')
    return hmac
  }

var movimientosV2JSON = require('./movimientosv2.json');

var requestjson = require('request-json');

var urlClientesMLab = "https://api.mlab.com/api/1/databases/jpedraza/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlClientesMLab);

var urlMLabRaiz = "https://api.mlab.com/api/1/databases/jpedraza/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;
//var usuarioMLabRaiz;

 
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res){
  //res.send('Hemos recibido su petición get');
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.post('/', function(req, res){
  res.send('Hemos recibido su petición post')
})

app.put('/', function(req, res){
  res.send('Hemos recibido su petición put')
})

app.delete('/', function(req, res){
  res.send('Hemos recibido su petición delete')
})

app.get('/v1/clientes/:idcliente', function(req, res){
  res.send('Aquí tiene el cliente número:' + req.params.idcliente);
})

app.post('/v1/clientes/:idcliente', function(req, res){
  res.send('Aquí agrega el cliente número:' + req.params.idcliente);
})

app.delete('/v1/clientes/:idcliente', function(req, res){
  res.send('Aquí borra el cliente número:' + req.params.idcliente);
})

app.get ('/v1/movimientos', function(req, res) {
  res.sendfile('movimientosv1.json');
})


app.get('/v2/movimientos', function(req,res){
  res.json(movimientosV2JSON);
})

app.get('/v2/movimientos/:index', function(req,res){
  console.log(req.param.index);
  res.json(movimientosV2JSON[req.params.index-1]);
})

app.get('/v2/movimientosquery', function(req, res){
  console.log(req.query);
  res.send('recibido');
})

app.post('/v2/movimientos', function(req, res){
  var nuevo = req.body
  nuevo.id = movimientosV2JSON.length + 1
  movimientosV2JSON.push(nuevo)
  res.send('movimiento dado de alta');
})

app.put('/v2/movimientos', function(req,res){
  res.send('Hemos recibido su petición de actualización de movimientos')
})

app.get('/v3/Clientes', function(req,res) {

  clienteMLab.get('', function(err, resM, body){
    if (err){
      console.log(body);
    }else{
      res.send(body);
  }
  })
})

app.post('/v3/Clientes', function(req, res){
  clienteMLab.post('',req.body, function(err, resM, body) {
    res.send(body);
    console.log(body);
  })
})


app.post('/v4/login', function(req,res){
  var passEncrypt = encriptar(req.body.email, req.body.password)
  var email = req.body.email;
  var password = passEncrypt; //req.body.password;
  var query = 'q={"email":"' + email + '",  "password":"' + password + '"}';

  usuarioMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);

  usuarioMLabRaiz.get ('', function(err, resM, body){
    if (!err){
      if (body.length == 1) { //Login OK
        res.status(200).send('Usuario logueado');
      } else {
        res.status(404).send('Usuario no existe, Regístrese');
      }
    }else {
      console.log(body);
    }
  })
})
